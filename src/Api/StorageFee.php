<?php

namespace WarehouseX\ClFee\Api;

use WarehouseX\ClFee\Model\StorageFee as StorageFeeModel;

class StorageFee extends AbstractAPI
{
    /**
     * Retrieves the collection of StorageFee resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'calcMethod'	string
     *                       'calcMethod[]'	array
     *                       'calcCycle'	string
     *                       'calcCycle[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'warehouseId'	integer
     *                       'warehouseId[]'	array
     *                       'zoneTypeId'	integer
     *                       'zoneTypeId[]'	array
     *                       'unitPrice'	number
     *                       'unitPrice[]'	array
     *                       'userId'	integer
     *                       'userId[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[warehouseId]'	string
     *                       'order[zoneTypeId]'	string
     *                       'order[unitPrice]'	string
     *                       'order[userId]'	string
     *                       'order[status]'	string
     *                       'order[currencyCode]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return StorageFeeModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getStorageFeeCollection',
        'GET',
        'api/cl-fee/storage_fees',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a StorageFee resource.
     *
     * @param StorageFeeModel $Model The new StorageFee resource
     *
     * @return StorageFeeModel
     */
    public function postCollection(StorageFeeModel $Model): StorageFeeModel
    {
        return $this->request(
        'postStorageFeeCollection',
        'POST',
        'api/cl-fee/storage_fees',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a StorageFee resource.
     *
     * @param string $id Resource identifier
     *
     * @return StorageFeeModel|null
     */
    public function getItem(string $id): ?StorageFeeModel
    {
        return $this->request(
        'getStorageFeeItem',
        'GET',
        "api/cl-fee/storage_fees/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the StorageFee resource.
     *
     * @param string          $id    Resource identifier
     * @param StorageFeeModel $Model The updated StorageFee resource
     *
     * @return StorageFeeModel
     */
    public function putItem(string $id, StorageFeeModel $Model): StorageFeeModel
    {
        return $this->request(
        'putStorageFeeItem',
        'PUT',
        "api/cl-fee/storage_fees/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the StorageFee resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteStorageFeeItem',
        'DELETE',
        "api/cl-fee/storage_fees/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the StorageFee resource.
     *
     * @param string $id Resource identifier
     *
     * @return StorageFeeModel
     */
    public function patchItem(string $id): StorageFeeModel
    {
        return $this->request(
        'patchStorageFeeItem',
        'PATCH',
        "api/cl-fee/storage_fees/$id",
        null,
        [],
        []
        );
    }
}
