<?php

namespace WarehouseX\ClFee\Api;

use WarehouseX\ClFee\Model\OutboundFee as OutboundFeeModel;

class OutboundFee extends AbstractAPI
{
    /**
     * Retrieves the collection of OutboundFee resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'outboundType'	string
     *                       'outboundType[]'	array
     *                       'currencyCode'	string
     *                       'currencyCode[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'warehouseId'	integer
     *                       'warehouseId[]'	array
     *                       'unitPrice'	number
     *                       'unitPrice[]'	array
     *                       'userId'	integer
     *                       'userId[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[warehouseId]'	string
     *                       'order[unitPrice]'	string
     *                       'order[userId]'	string
     *                       'order[status]'	string
     *                       'order[outboundType]'	string
     *                       'order[currencyCode]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return OutboundFeeModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getOutboundFeeCollection',
        'GET',
        'api/cl-fee/outbound_fees',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a OutboundFee resource.
     *
     * @param OutboundFeeModel $Model The new OutboundFee resource
     *
     * @return OutboundFeeModel
     */
    public function postCollection(OutboundFeeModel $Model): OutboundFeeModel
    {
        return $this->request(
        'postOutboundFeeCollection',
        'POST',
        'api/cl-fee/outbound_fees',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a OutboundFee resource.
     *
     * @param string $id Resource identifier
     *
     * @return OutboundFeeModel|null
     */
    public function getItem(string $id): ?OutboundFeeModel
    {
        return $this->request(
        'getOutboundFeeItem',
        'GET',
        "api/cl-fee/outbound_fees/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the OutboundFee resource.
     *
     * @param string           $id    Resource identifier
     * @param OutboundFeeModel $Model The updated OutboundFee resource
     *
     * @return OutboundFeeModel
     */
    public function putItem(string $id, OutboundFeeModel $Model): OutboundFeeModel
    {
        return $this->request(
        'putOutboundFeeItem',
        'PUT',
        "api/cl-fee/outbound_fees/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the OutboundFee resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteOutboundFeeItem',
        'DELETE',
        "api/cl-fee/outbound_fees/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the OutboundFee resource.
     *
     * @param string $id Resource identifier
     *
     * @return OutboundFeeModel
     */
    public function patchItem(string $id): OutboundFeeModel
    {
        return $this->request(
        'patchOutboundFeeItem',
        'PATCH',
        "api/cl-fee/outbound_fees/$id",
        null,
        [],
        []
        );
    }
}
