<?php

namespace WarehouseX\ClFee\Api;

use WarehouseX\ClFee\Model\UnloadFee as UnloadFeeModel;

class UnloadFee extends AbstractAPI
{
    /**
     * Retrieves the collection of UnloadFee resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'unloadType'	string
     *                       'unloadType[]'	array
     *                       'currencyCode'	string
     *                       'currencyCode[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'warehouseId'	integer
     *                       'warehouseId[]'	array
     *                       'unitPrice'	number
     *                       'unitPrice[]'	array
     *                       'userId'	integer
     *                       'userId[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[warehouseId]'	string
     *                       'order[unitPrice]'	string
     *                       'order[userId]'	string
     *                       'order[status]'	string
     *                       'order[unloadType]'	string
     *                       'order[currencyCode]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return UnloadFeeModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getUnloadFeeCollection',
        'GET',
        'api/cl-fee/unload_fees',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a UnloadFee resource.
     *
     * @param UnloadFeeModel $Model The new UnloadFee resource
     *
     * @return UnloadFeeModel
     */
    public function postCollection(UnloadFeeModel $Model): UnloadFeeModel
    {
        return $this->request(
        'postUnloadFeeCollection',
        'POST',
        'api/cl-fee/unload_fees',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a UnloadFee resource.
     *
     * @param string $id Resource identifier
     *
     * @return UnloadFeeModel|null
     */
    public function getItem(string $id): ?UnloadFeeModel
    {
        return $this->request(
        'getUnloadFeeItem',
        'GET',
        "api/cl-fee/unload_fees/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the UnloadFee resource.
     *
     * @param string         $id    Resource identifier
     * @param UnloadFeeModel $Model The updated UnloadFee resource
     *
     * @return UnloadFeeModel
     */
    public function putItem(string $id, UnloadFeeModel $Model): UnloadFeeModel
    {
        return $this->request(
        'putUnloadFeeItem',
        'PUT',
        "api/cl-fee/unload_fees/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the UnloadFee resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteUnloadFeeItem',
        'DELETE',
        "api/cl-fee/unload_fees/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the UnloadFee resource.
     *
     * @param string $id Resource identifier
     *
     * @return UnloadFeeModel
     */
    public function patchItem(string $id): UnloadFeeModel
    {
        return $this->request(
        'patchUnloadFeeItem',
        'PATCH',
        "api/cl-fee/unload_fees/$id",
        null,
        [],
        []
        );
    }
}
