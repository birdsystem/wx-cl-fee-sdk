<?php

namespace WarehouseX\ClFee;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getOutboundFeeCollection' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\OutboundFee[]',
        ],
        'postOutboundFeeCollection' => [
            '201.' => 'WarehouseX\\ClFee\\Model\\OutboundFee',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getOutboundFeeItem' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\OutboundFee',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putOutboundFeeItem' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\OutboundFee',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteOutboundFeeItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchOutboundFeeItem' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\OutboundFee',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getStorageFeeCollection' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\StorageFee[]',
        ],
        'postStorageFeeCollection' => [
            '201.' => 'WarehouseX\\ClFee\\Model\\StorageFee',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getStorageFeeItem' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\StorageFee',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putStorageFeeItem' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\StorageFee',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteStorageFeeItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchStorageFeeItem' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\StorageFee',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getUnloadFeeCollection' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\UnloadFee[]',
        ],
        'postUnloadFeeCollection' => [
            '201.' => 'WarehouseX\\ClFee\\Model\\UnloadFee',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getUnloadFeeItem' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\UnloadFee',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putUnloadFeeItem' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\UnloadFee',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteUnloadFeeItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchUnloadFeeItem' => [
            '200.' => 'WarehouseX\\ClFee\\Model\\UnloadFee',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
