<?php

namespace WarehouseX\ClFee\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * UnloadFee.
 */
class UnloadFee extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $warehouseId = null;

    /**
     * @var string
     */
    public $unloadType = null;

    /**
     * @var float
     */
    public $unitPrice = null;

    /**
     * @var string
     */
    public $currencyCode = null;

    /**
     * @var string
     */
    public $beginTime = null;

    /**
     * @var string|null
     */
    public $endTime = null;

    /**
     * @var int
     */
    public $userId = null;

    /**
     * @var string|null
     */
    public $status = null;

    /**
     * @var string|null
     */
    public $createTime = null;

    /**
     * @var string|null
     */
    public $updateTime = null;

    /**
     * @var string|null
     */
    public $warehouseName = null;

    /**
     * @var string|null
     */
    public $userName = null;
}
